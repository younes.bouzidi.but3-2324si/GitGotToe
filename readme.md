
```
  ____ _ _      ____       _     _____
 / ___(_) |_   / ___| ___ | |_  |_   _|__   ___
| |  _| | __| | |  _ / _ \| __|   | |/ _ \ / _ \
| |_| | | |_  | |_| | (_) | |_    | | (_) |  __/
 \____|_|\__|  \____|\___/ \__|   |_|\___/ \___|
```

# Objective
Learn Git in a fun and interactive way with GitGotToe, a Tic Tac Toe game designed to help you grasp the **basics of Git without having to focus on complex code**.

# Getting Started

To begin, follow these steps:

1. **Fork the Repository:** Start by forking this GitGotToe repository to your GitLab account. You can do this by clicking the "Fork" button at the top right corner of this page.

2. **Add a Collaborator:** To set up a two-player game, you'll need to add a collaborator to your forked repository. Your collaborator will be your opponent in the game. On your forked repository page, go to "Settings" > "Members" and add the GitLab account of your collaborator as a member with the necessary permissions.

3. **Clone the Repository:** Clone your forked repository to your local machine using the following Git command:
```
git clone <YOUR-repository-url>
```


4. **Configure Your Git Identity:** Before you start making commits, it's essential to configure your Git identity
```
git config --global user.name "Your Name"
git config --global user.email "youremail@example.com"
```

**Warning:** In the case of a public repository, it's advisable not to use your personal email address. We recommend using the email provided by your Git hosting platform.

Now, you and your collaborator are ready to start playing GitGotToe!

# Let's Play !
:bulb: When using Git, you should use these commands very, **very often** : `git status` and `git log` :bulb:

**Player 1:**
1. Copy the game board to your own file:
```
cp grid.txt part_01
```
2. Edit your file using a text editor of your choice. For example:
```
vim part_01
```
3. Add your changes to the staging area:
```
git add part_01
```
4. Commit your move with a descriptive message:
```
git commit -m "Player 1: Your message here"
```

If you're unsure about what to write in your commit message, don't worry. We've got you covered. Check out this resource on commit message conventions at [https://www.conventionalcommits.org/](<https://www.conventionalcommits.org/>) for guidance and best practices.


5. Push your changes to the repository:
```
git push
```

**Player 2:**
:bulb: You should use these commands very, **very often** : `git status` and `git log` :bulb:

1. Before making your move, make sure to sync with the latest changes from Player 1 by pulling:
```
git pull
```

2. Follow the same steps as Player 1, editing the file, adding changes to the staging area, committing your move, and pushing the changes.

Repeat these steps until you either win the game or it ends in a draw.

# What's Next?
Repeat these steps 5 to 10 times to become comfortable with the Git commands. Afterward, explore the different branches in the repository using the command:
```
git branch --all
```
Congratulations! You've just completed Level 0. Now, advance to Level 1 with:
```
git switch level_1
```

Keep learning Git with this just boring game, and most importantly, remember to RTFM ! [Git Official Documentation](https://git-scm.com/doc).
